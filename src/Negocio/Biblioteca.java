/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template

Enunciado:
https://docs.google.com/document/d/1cAXPd_GRT8PyyP85mc52qqf2gRnbILl9/edit?usp=sharing&ouid=107614452311706692726&rtpof=true&sd=true
 */
package Negocio;

import Modelo.Estudiante;
import Modelo.Facultad;
import Modelo.Mensaje;
import Modelo.Solicitud;
import Util.seed.ArchivoLeerURL;
import Util.seed.ListaCD;

/**
 *
 * @author DOCENTE
 */
public class Biblioteca {

    private ListaCD<Facultad> facultades = new ListaCD();
    private ListaCD<Solicitud> solicitudes = new ListaCD();
    private ListaCD<Estudiante> estudiantes = new ListaCD();

    public Biblioteca() {
    }

    public void cargarSolicitudes(String urlSolicitudes) {
        ArchivoLeerURL archivo = new ArchivoLeerURL(urlSolicitudes);
        Object datos[] = archivo.leerArchivo();

        for (int i = 1; i < datos.length; i++) {

            String fila = datos[i].toString();
            String[] info = fila.split(";");

            // COMPLETAR
        }

    }

    public void cargarFacultades(String urlFacultades) {

        ArchivoLeerURL archivo = new ArchivoLeerURL(urlFacultades);
        Object datos[] = archivo.leerArchivo();

        for (int i = 1; i < datos.length; i++) {

            String fila = datos[i].toString();
            String[] info = fila.split(";");

            int codeFac = Integer.parseInt(info[0]);
            String nombreFac = info[1];

            Facultad facultad = new Facultad(codeFac, nombreFac);

            this.facultades.insertarFinal(facultad);
        }
    }

    public void cargarEstudiantes(String urlEstudiantes) {

    }

    public void cargarInventario(String urlInventario) {

    }

    public ListaCD<Mensaje> procesarSolicitudes() {
        return null;
    }

    public String getFacultades() {
        String msg = facultades.toString();
        return msg;
    }

    // public void crearPDF(ListaCD<Mensaje> resultado) {

    // }

}
