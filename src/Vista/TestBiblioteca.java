/*
 * Enunciado:
https://docs.google.com/document/d/1cAXPd_GRT8PyyP85mc52qqf2gRnbILl9/edit?usp=sharing&ouid=107614452311706692726&rtpof=true&sd=true
 */
package Vista;

import Negocio.Biblioteca;

/**
 *
 * @author DOCENTE
 */
public class TestBiblioteca {
    public static void main(String[] args) {
        String urlFacultades = "https://gitlab.com/madarme/archivos-persistencia/raw/master/facultad.csv";
        String urlEstudiantes = "https://gitlab.com/madarme/archivos-persistencia/raw/master/estudiantes.csv";
        String urlInventario = "https://gitlab.com/madarme/archivos-persistencia/-/raw/master/inventario.csv?ref_type=heads";
        String urlSolicitudes = "https://gitlab.com/madarme/archivos-persistencia/raw/master/solicitudes.csv";
        Biblioteca biblioteca = new Biblioteca();
        biblioteca.cargarFacultades(urlFacultades);
        // biblioteca.cargarEstudiantes(urlEstudiantes);
        // biblioteca.cargarInventario(urlInventario);
        // biblioteca.cargarSolicitudes(urlSolicitudes);
        // ListaCD<Mensaje> r=biblio.procesarSolicitudes();
        // System.out.println(r.toString());
        // biblio.crearPDF(r);

        System.out.println(biblioteca.getFacultades());
    }

}
