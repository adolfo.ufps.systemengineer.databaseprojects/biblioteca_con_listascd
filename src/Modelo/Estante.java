/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Modelo;

import Util.seed.Pila;

/**
 * APARENTEMENTE ESTA CLASE SOBRA...
 * 
 * @author DOCENTE
 */
public class Estante implements Comparable {

    private int codigo;

    private Pila<Libro> libros;

    public Estante(int codigo) {
        this.codigo = codigo;
        libros = new Pila();
    }

    @Override
    public int compareTo(Object o) {
        if (!verificatorObjects(o)) {
            throw new RuntimeException("Se han intentado comparar dos objetos distintos...");
        }
        Estante x = (Estante) o;
        return this.getCodigo() - x.getCodigo();
    }

    @Override
    public boolean equals(Object o) {
        if (verificatorObjects(o)) {
            Estante x = (Estante) o;
            if (this.getCodigo() == x.getCodigo()) {
                return true;
            }
            return false;
        }
        return false;
    }

    public boolean verificatorObjects(Object o) {
        if (o != null) {
            if (o.getClass() == this.getClass()) {
                return true;
            }
            return false;
        }
        return false;
    }

    /**
     * Pasar la pila original a una copia, asegurarla, y trabajar sobre la original
     * Boolean para comprobar si se puede o no entregar el libro
     */

    public void adicionarLibro() {

    }

    public void removerLibros() {

    }

    public int getCodigo() {
        return codigo;
    }
}
