/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Modelo;

import Util.seed.Pila;

/**
 *
 * @author DOCENTE
 */
public class Facultad {

    private int codigo;
    private String nombre;

    private Pila<Libro> estantes;

    public Facultad(int codigo, String nombre) {
        this.codigo = codigo;
        this.nombre = nombre;
        estantes = new Pila<Libro>();
    }

    public int getCodigo() {
        return codigo;
    }

    public String getNombre() {
        return nombre;
    }

    @Override
    public String toString() {
        return this.nombre;
    }
}
