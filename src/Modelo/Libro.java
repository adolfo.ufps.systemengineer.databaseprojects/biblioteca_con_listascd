/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Modelo;

/**
 *
 * @author DOCENTE
 */
public class Libro implements Comparable {

    private String nombre;
    private int codigo;

    public Libro(int codigo, String nombre) {
        this.codigo = codigo;
        this.nombre = nombre;
    }

    public int getCodigo() {
        return codigo;
    }

    public String getNombre() {
        return nombre;
    }

    @Override
    public int compareTo(Object o) {
        throw new UnsupportedOperationException("Unimplemented method 'compareTo'");
    }

    @Override
    public boolean equals(Object o) {
        throw new UnsupportedOperationException("Unimplemented method 'compareTo'");
    }

    /**
     * Retorna si el objeto This y el objeto O se pueden comparar
     * 
     * @param o El objeto con el que se va a comparar
     * @return True si se puede, False si no
     */
    public boolean verificatorObjects(Object o) {
        if (o != null) {
            if (o.getClass() == this.getClass()) {
                return true;
            }
            return false;
        }
        return false;
    }
}
